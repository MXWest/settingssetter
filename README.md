# Settings Setter

Settings Setter builds Python dictionaries from YAML configuration files.

## YAML Configuration

Settings setter parses a YAML file, treating each entity as one of 3 types:

- A dictionary: a set of key-value pairs
- A list: a sequence of items
- A value: a _string_ that is either an environment variable _or_ a static value. Settings setter first tries to
find an environment variable named named _string_. If none is found, the value is treated as a literal. The value is
assigned to the `settings` dictionary at the appropriate location.

### Of Interest: `utils/yaml_setter.py`

The methods that actually do they heavy lifting. Although pyyaml is used to parse the yaml, advanced features
of pyyaml are specifically avoided.

## Usage
 
1. Set up your configuration yaml file.
1. In your `settings.py` file, import the `yaml_setter` method from `utils.yaml_setter`
1. Call `yaml_setter`, optionally passing a filename.
1. `yaml_setter` returns a `settings`-style dictionary.

## Example

Run `app.py` to examine the provided examples:

1. `settings.yaml` demonstrate that default filename is settings.yaml
1. `settings_demo.yaml` a more sophisticated configuration


## Pros
- YAML is easily edited, maintained and read.
- YAML serves developers as an easy-to-follow table of contents for all settings in a given app.
- Ensures consistency in assignment across Services.
- PyCharm IDE collapses YAML to reduce distraction (as will any sensible editor).
- Separates _compound_ settings (combinations of multiple configuration options) which requires coding, from 
_static_ configuration values which do not.
- Significant reduction of noise in settings.py files
- Works with all currently existing settings for all services across all deployment environments without change - and
these environments already work.
- Defaults change from inline code decisions to environment setup files. Local combinations of services are easily
managed for developers.


## Cons
- Nobody likes configuration, and this is configuration


## Comparison

This section compares the Code required to assign values to a 'settings_demo' dictionary using `SettingsSetter`
to inline in `settings.py`.

### Assignment using SettingsSetter
#### YAML

```
settings_setter_demo:
    parse_path_env_variable: PATH
    list_of_things:
      - HOME
      - 'A literal value'
      - TMPDIR
      - dict_inside_a_list:
          x: 'X Literal'
          y: TMPDIR
          z: '01'
    dict_of_dicts:
      a: '01'
      b: USER
      c: 'Thing C'
```      

#### settings.py
```
from utils.yaml_setter import yaml_setter

settings = yaml_setter()
```

### Assignment using inline settings.py
```
settings = {
    'settings_setter_demo': {
        'list_of_things': [
            os.environ.setdefault("HOME", "HOME"),
            'A literal value',
            os.environ.setdefault("TMPDIR", "TMPDIR"),
            {'dict_inside_a_list': {
                'x': 'X Literal',
                'y': os.environ.setdefault("TMPDIR", "TMPDIR"),
                'z': '01'}}
        ],
        'dict_of_dicts': {
            'a': '01',
            'b': os.environ.setdefault("USER", "USER"),
            'c': 'Thing C'
        },
        'parse_path_env_variable': os.environ.setdefault("PATH", "PATH")
    }
}
```
