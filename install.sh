#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -d ${DIR}/venv ]; then
    virtualenv -p python2.7 "${DIR}/venv"
    echo "export PYTHONPATH=$DIR" >> $DIR/venv/bin/activate
fi
source ${DIR}/venv/bin/activate
pip install -r ${DIR}/requirements.txt
