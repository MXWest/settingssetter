from settings import settings, settings_demo
import pprint


def settings_demo_dump():
    for title, s in {"Default": settings, "Expanded Demo": settings_demo}.iteritems():
        print "=== Begin {} ===".format(title)
        pprint.PrettyPrinter(indent=2).pprint(s)
        print "===   End {} ===".format(title)


if __name__ == "__main__":
    settings_demo_dump()
