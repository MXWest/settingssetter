from utils.yaml_setter import yaml_setter

# Normally, we'd load a configuration from the default location
settings = yaml_setter()

# Here we load from a different file
settings_demo = yaml_setter(filename='settings_demo.yaml')
