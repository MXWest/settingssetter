from unittest import TestCase
from mock import patch, mock_open
from settings_setter.utils.yaml_setter import yaml_setter, _select_generator
from os import environ

TEST_YAML_DOC = """
test:
    path: PATH
    list_of_things:
      - 'list element one'
      - 'list element two'
      - PATH
      - dict_inside_a_list:
          x: 'X Literal'
          y: PATH
          z: 'Z Literal'
    dict_of_things:
      a: 'Thing A'
      b: PATH
      c: 'Thing C'
"""


class YamlSetterTests(TestCase):
    def test_yaml_setter(self):
        path_env = environ.get("PATH")
        with patch('settings_setter.utils.yaml_setter.open', mock_open(read_data=TEST_YAML_DOC)):
            test_setting = yaml_setter()

        self.assertEqual(test_setting['test']['path'], path_env)
        self.assertEqual(test_setting['test']['list_of_things'][0], 'list element one')
        self.assertEqual(test_setting['test']['list_of_things'][2], path_env)
        self.assertEqual(test_setting['test']['list_of_things'][3]['dict_inside_a_list']['y'], path_env)
        self.assertEqual(test_setting['test']['dict_of_things']['a'], 'Thing A')
        self.assertEqual(test_setting['test']['dict_of_things']['b'], path_env)

    def test__select_generator_returns_empty_for_strings(self):
        self.assertEqual([], _select_generator("This is not a dict or a list"))
