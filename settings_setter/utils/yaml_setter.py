from os import environ
import yaml


def _select_generator(array):
    """
    Selects an appropriate key-value generator based on whether we get a dict or a list
    :param array: a dict or a list
    :return: generator, or, an empty list
    """
    if isinstance(array, dict):
        return ((k, v) for k, v in array.iteritems())
    return ((k, v) for k, v in enumerate(array)) if isinstance(array, list) else []


def _load_array_values(array):
    """
    Load values in the list or dict represented by array using environment
    variable replacement, or by treating the value as a literal.
    :param array: a list or dict with values to be loaded
    :return: n/a
    """
    for k, v in _select_generator(array):
        if isinstance(v, (dict, list)):
            _load_array_values(v)
        else:
            array[k] = environ.setdefault(v, v) if isinstance(v, str) else v


def yaml_setter(filename='settings.yaml'):
    """
    Loads a yaml configuration files and transforms values
    :param filename: Optional filename (default is settings.yaml)
    :return: Dictionary of configuration
    """
    with open(filename, 'r') as f:
        settings_array = yaml.load(f)
    _load_array_values(settings_array)
    return settings_array
